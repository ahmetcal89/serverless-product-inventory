# Product Inventory Application API
**Maintained by:** [Ahmet Caliskan](ahmet.cal@hotmail.com.tr)

## Tool Selection and Design Decisions
Tool selection basically done regarding job description;
 - Serverless Application Model (ApiGW + Lambda + SQS + DynamoDB)
 - Java 11 and Spring Boot
 - In terms of High Availability/High Scalability and Cost Reduction decided to go Serverless
 - Since we need to separate Product Inventory and Supply Chain concerns I decided to use SQS to handle that.

## Improvement Points
 - Instead of DynamoDB, elastic cache redis would work much more efficient
 - Spring Boot is not good choice for Lambda in terms of Cold Start-up Times (Used because of JD)
 - Testing (In terms of tight deadlines seems little weak and could be improved)
 - Authentication / Authorization
 

## Architecture
![ExecutionTriggerArchitecture](images/deployment_diagram.jpeg)

## Tech Stack
 - Spring Boot
 - DynamoDB
 - SQS
 - Lambda

## Pipeline
  - build
  - unitTes
  - integrationTest
  - featureTest
  - dev
  - staging
  - prod
![Pipeline](images/pipeline.png)

Note: CI Stages are running by default but CD Stages configured as manual.

## Product Inventory API
[DEV API URL](https://g2ozflwq37.execute-api.eu-central-1.amazonaws.com/dev/)

[PROD API URL](https://mv65j08tga.execute-api.eu-central-1.amazonaws.com/prod/)


## Local Development
### Preconditions
- aws cli
- npm
- Serverless Framework

### Required Env Variables
- environment
- accountId
- region
- appointmentsQueName

### Build
- gradle build
### Unit Test
- gradle unitTest
### Integration Test
- gradle integrationTest

### Deploy
- sls deploy --stage dev
### Destroy
- sls remove --stage dev

### Open API 3.0 Documentation

``` json
{
  "openapi" : "3.0.1",
  "info" : {
    "title" : "dev-product-inventory",
    "version" : "2022-03-04T06:44:54Z"
  },
  "servers" : [ {
    "url" : "https://opbq0zso7j.execute-api.eu-central-1.amazonaws.com/{basePath}",
    "variables" : {
      "basePath" : {
        "default" : "/dev"
      }
    }
  } ],
  "paths" : {
    "/products/{product_id}" : {
      "get" : {
        "parameters" : [ {
          "name" : "product_id",
          "in" : "path",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        } ]
      },
      "delete" : {
        "parameters" : [ {
          "name" : "product_id",
          "in" : "path",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        } ]
      },
      "options" : {
        "parameters" : [ {
          "name" : "product_id",
          "in" : "path",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "200 response",
            "headers" : {
              "Access-Control-Allow-Origin" : {
                "schema" : {
                  "type" : "string"
                }
              },
              "Access-Control-Allow-Methods" : {
                "schema" : {
                  "type" : "string"
                }
              },
              "Access-Control-Allow-Headers" : {
                "schema" : {
                  "type" : "string"
                }
              }
            },
            "content" : { }
          }
        }
      }
    },
    "/products" : {
      "get" : { },
      "post" : { },
      "options" : {
        "responses" : {
          "200" : {
            "description" : "200 response",
            "headers" : {
              "Access-Control-Allow-Origin" : {
                "schema" : {
                  "type" : "string"
                }
              },
              "Access-Control-Allow-Methods" : {
                "schema" : {
                  "type" : "string"
                }
              },
              "Access-Control-Allow-Headers" : {
                "schema" : {
                  "type" : "string"
                }
              }
            },
            "content" : { }
          }
        }
      }
    }
  },
  "components" : { }
}
```

[Swagger Collection](scripts/api_docs.json)

More API info could be found on swagger-ui by running locally and navigate http://localhost:8080/swagger-ui.html
