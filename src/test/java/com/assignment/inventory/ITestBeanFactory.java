package com.assignment.inventory;

import com.assignment.inventory.repository.dao.ProductsDao;
import com.assignment.inventory.sqs.ProductRequestQue;
import org.junit.jupiter.api.Tag;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

@TestConfiguration
@ActiveProfiles("test")
@Tag(Category.INTEGRATION_TEST)
public class ITestBeanFactory {
    private static final ProductRequestQue MOCK_PRODUCT_REQUEST_QUE = Mockito.mock(ProductRequestQue.class);
    private static final ProductsDao MOCK_PRODUCTS_DAO = Mockito.mock(ProductsDao.class);

    @Bean
    public static ProductRequestQue getMockProductRequestQue() {
        return MOCK_PRODUCT_REQUEST_QUE;
    }

    @Bean
    public static ProductsDao getMockProductsDao() {
        return MOCK_PRODUCTS_DAO;
    }

}
