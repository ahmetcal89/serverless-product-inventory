package com.assignment.inventory;

public final class Category {
    public static final String INTEGRATION_TEST = "IntegrationTest";
    public static final String UNIT_TEST = "UnitTest";
}
