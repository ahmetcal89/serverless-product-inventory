package com.assignment.inventory.service.impl;

import com.assignment.inventory.Category;
import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.dao.ProductsDao;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.sqs.ProductRequestQue;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.assignment.inventory.supply.SupplyChain;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpServerErrorException;

import java.util.LinkedList;
import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
@Tag(Category.UNIT_TEST)
class DefaultSupplyChainServiceTest {

    @Mock
    private SupplyChain supplyChain;
    @Mock
    private ProductsDao productsDao;
    @Mock
    private ProductRequestQue productRequestQue;
    @InjectMocks
    private DefaultSupplyChainService sut;

    @Test
    public void testProcessCreateProductRequest() {
        // Arrange
        CreateProductRequest request = new CreateProductRequest("1", new ProductForm("1", 2, 2.0));

        // Act
        sut.processCreateProductRequest(request);

        // Assert
        Mockito.verify(supplyChain, Mockito.times(1)).saveProduct(request.getProductForm());
        Mockito.verify(productsDao, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void testProcessCreateProductRequest_serverNotAvailable() {
        // Arrange
        CreateProductRequest request = new CreateProductRequest("1", new ProductForm("1", 2, 2.0));
        Mockito.when(supplyChain.saveProduct(request.getProductForm())).thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

        // Act
        sut.processCreateProductRequest(request);

        // Assert
        Mockito.verify(productRequestQue, Mockito.times(1)).send(Mockito.any(RequestTypes.class), Mockito.any(CreateProductRequest.class), Mockito.anyInt());

    }

    @Test
    public void testProcessDeleteProductRequest() {
        // Arrange
        DeleteProductRequest request = new DeleteProductRequest("1", "2");

        // Act
        sut.processDeleteProductRequest(request);

        // Assert
        Mockito.verify(supplyChain, Mockito.times(1)).deleteProduct(request.getProductId());
        Mockito.verify(productsDao, Mockito.times(1)).deleteById(request.getProductId());
    }


    @Test
    public void testSyncProducts() {
        // Arrange
        List<Product> testProducts = new LinkedList<>();
        Mockito.when(supplyChain.listProducts()).thenReturn(testProducts);

        // Act
        sut.syncProducts();

        // Assert
        Mockito.verify(supplyChain, Mockito.times(1)).listProducts();
        Mockito.verify(productsDao, Mockito.times(0)).save(Mockito.any());
    }

}