package com.assignment.inventory.service.impl;

import com.assignment.inventory.Category;
import com.assignment.inventory.exception.InternalServerException;
import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.dao.ProductsDao;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.sqs.ProductRequestQue;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@Tag(Category.UNIT_TEST)
class DefaultProductsServiceTest {

    @Mock
    private ProductsDao productsDao;
    @Mock
    private ProductRequestQue productRequestQue;
    @InjectMocks
    private DefaultProductsService sut;

    @Test
    public void testListProducts() {
        // Arrange
        List<Product> testProducts = new LinkedList<>();
        Mockito.when(productsDao.findAll()).thenReturn(testProducts);

        // Act
        List<Product> result = sut.listProducts();

        // Assert
        assertEquals(result, testProducts);
    }

    @Test
    public void testCreateProductRequest() {
        // Arrange
        ProductForm productForm = new ProductForm("1", 5, 3.3);
        Mockito.when(productRequestQue.send(
                Mockito.any(RequestTypes.class), Mockito.any(CreateProductRequest.class), Mockito.anyInt())).thenReturn(Optional.of(true));

        // Act
        String requestId = sut.createProductRequest(productForm);

        // Assert
        assertNotNull(requestId);
    }

    @Test
    public void testCreateProductRequest_sqsException() {
        // Arrange
        ProductForm productForm = new ProductForm("1", 5, 3.3);
        Mockito.when(productRequestQue.send(
                Mockito.any(RequestTypes.class), Mockito.any(CreateProductRequest.class), Mockito.anyInt())).thenReturn(Optional.empty());

        // Act & Assert
        Exception exception = assertThrows(InternalServerException.class, () -> {
            String requestId = sut.createProductRequest(productForm);
        });
    }

    @Test
    public void testGetById() {
        // Arrange
        String testId = "1";
        Product testProduct = new Product(testId, "aaa", 1, 2.0);
        Mockito.when(productsDao.findById(testId)).thenReturn(Optional.of(testProduct));

        // Act
        Product result = sut.getById(testId);

        // Assert
        assertEquals(result.getId(), testId);
    }

    @Test
    public void testDeleteProductRequest() {
        // Arrange
        String testId = "1";
        Mockito.when(productRequestQue.send(
                Mockito.any(RequestTypes.class), Mockito.any(DeleteProductRequest.class), Mockito.anyInt())).thenReturn(Optional.of(true));

        // Act
        String requestId = sut.deleteProductRequest(testId);

        // Assert
        assertNotNull(requestId);
    }

    @Test
    public void testDeleteProductRequest_sqsException() {
        // Arrange
        ProductForm productForm = new ProductForm("1", 5, 3.3);
        Mockito.when(productRequestQue.send(
                Mockito.any(RequestTypes.class), Mockito.any(DeleteProductRequest.class), Mockito.anyInt())).thenReturn(Optional.empty());

        // Act & Assert
        Exception exception = assertThrows(InternalServerException.class, () -> {
            String requestId = sut.createProductRequest(productForm);
        });
    }
}