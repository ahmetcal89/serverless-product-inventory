package com.assignment.inventory.controller;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.assignment.inventory.Category;
import com.assignment.inventory.StreamLambdaHandler;
import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("test")
@Tag(Category.INTEGRATION_TEST)
class EventControllerTest {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private EventController sut;

    private final String proxyRequest = "{\n" +
            "    \"Records\": [\n" +
            "        {\n" +
            "            \"messageId\": \"aee471c7-e117-4212-8263-d10255c685d7\",\n" +
            "            \"receiptHandle\": \"AQEBD5TAI64o7boes1yqWlvqJMsgDR8vjDHjmuWrGAwQBz+bk3W5TxUocoEGLI5NeFXKenWYfYLig8QV4kcBEyX4jTT3FvyxpBevuj9Mc22SWF3F2lrV/K2EPons0mHtLcfF/NvnoBW54A/NyaAOeMUOi5GksqRQsA7Sdjb2yX6oeI/RHZjTr2NH9aSQOEMcM3THB8dSh1TBjr1pFEpxYLcFSaF69+eDiHWzZzxRqLjaWr+op3WootdlT23AAhO+s1mgD/JD4KbbZfb0+iM4FbIRlhZ2gxsA/syXZGr/qnVLWR/ppCFYcHFR7tBB3ceAu3GuuP4kzkVnwQTA9P7B42Z8m8eHEcG9XS0jHt/R9b3XixzN0uXY1VAsSaJPjRlLyZhF4hwmC5h1BwmtzDhdsYCvLMdIcMIpA14ewwrXpdDvtN8=\",\n" +
            "            \"body\": \"{\\\"requestId\\\":\\\"ec2c0eaf-b607-4ecc-b338-fba8b3c56d5e\\\",\\\"productForm\\\":{\\\"name\\\":\\\"testing 123\\\",\\\"quantity\\\":5,\\\"price\\\":12.34}}\",\n" +
            "            \"attributes\": {\n" +
            "                \"ApproximateReceiveCount\": \"1\",\n" +
            "                \"SentTimestamp\": \"1646361808474\",\n" +
            "                \"SenderId\": \"AROAWC4BXCIGWMFWUQQ7M:product-inventory-dev-createProduct\",\n" +
            "                \"ApproximateFirstReceiveTimestamp\": \"1646361808478\"\n" +
            "            },\n" +
            "            \"messageAttributes\": {\n" +
            "                \"Type\": {\n" +
            "                    \"stringValue\": \"CREATE_PRODUCT_REQUEST\",\n" +
            "                    \"stringListValues\": [],\n" +
            "                    \"binaryListValues\": [],\n" +
            "                    \"dataType\": \"String\"\n" +
            "                }\n" +
            "            },\n" +
            "            \"md5OfMessageAttributes\": \"4717df7e18c822522110982d33081d52\",\n" +
            "            \"md5OfBody\": \"9bf4820b9c0313a5a8a022493a9e757b\",\n" +
            "            \"eventSource\": \"aws:sqs\",\n" +
            "            \"eventSourceARN\": \"arn:aws:sqs:eu-central-1:418494485005:product-inventory-dev-products-que\",\n" +
            "            \"awsRegion\": \"eu-central-1\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    private String eventBridge = "{\n" +
            "    \"version\": \"0\",\n" +
            "    \"id\": \"b4d7d291-00f8-dadc-a3d6-9a3d1b52039b\",\n" +
            "    \"detail-type\": \"Scheduled Event\",\n" +
            "    \"source\": \"aws.events\",\n" +
            "    \"account\": \"418494485005\",\n" +
            "    \"time\": \"2022-03-04T03:59:35Z\",\n" +
            "    \"region\": \"eu-central-1\",\n" +
            "    \"resources\": [\n" +
            "        \"arn:aws:events:eu-central-1:418494485005:rule/product-inventory-dev-SupplyChainServiceEventsRule-G8R43637HSUT\"\n" +
            "    ],\n" +
            "    \"detail\": {}\n" +
            "}";


    @Test
    public void testSqsEvent() throws JsonProcessingException {
        Map<String, String> result = sut.handleEvent(StreamLambdaHandler.AnnotatedSQSEvent.class.getName(), proxyRequest);

        assertNotNull(result);
        assertEquals(result.get("status"), "OK");
    }

    @Test
    public void testScheduledEvent() throws JsonProcessingException {
        Map<String, String> result = sut.handleEvent(StreamLambdaHandler.AnnotatedScheduledEvent.class.getName(), eventBridge);

        assertNotNull(result);
        assertEquals(result.get("status"), "OK");
    }


}