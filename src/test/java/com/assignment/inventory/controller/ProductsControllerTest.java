package com.assignment.inventory.controller;

import com.assignment.inventory.Category;
import com.assignment.inventory.ITestBeanFactory;
import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.model.Response;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Tag(Category.INTEGRATION_TEST)
@Slf4j
class ProductsControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkIfServletContextLoadedCorrectly() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
    }

    @Test
    public void testGetProductsEndpoint_listAllProducts() throws Exception {
        // Arrange
        List<Product> products = new LinkedList<>();
        Product product = new Product("1", "testName", 1, 2.2);
        products.add(product);
        Mockito.when(ITestBeanFactory.getMockProductsDao().findAll()).thenReturn(products);

        final MockHttpServletRequestBuilder requestBuilder =
                get("/products")
                        .contentType(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Assert
        Assert.assertNotNull(result);
        List<Product> resultArray = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<Product>>() {
                });
        Assert.assertNotNull(resultArray);
        Product receivedProduct = resultArray.get(0);
        assertEquals(product.getId(), receivedProduct.getId());
    }

    @Test
    public void testPostProductsEndpoint_createProductRequest() throws Exception {
        // Arrange
        ProductForm productForm = new ProductForm("testName", 1, 2.2);
        Mockito.when(ITestBeanFactory.getMockProductRequestQue().send(
                Mockito.any(RequestTypes.class), Mockito.any(CreateProductRequest.class), Mockito.anyInt())).thenReturn(Optional.of(true));

        final MockHttpServletRequestBuilder requestBuilder =
                post("/products")
                        .content(objectMapper.writeValueAsString(productForm))
                        .contentType(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isAccepted())
                .andReturn();

        // Assert
        Assert.assertNotNull(result);
        Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                Response.class);
        Assert.assertNotNull(response);
        assertEquals(response.getStatus(), "ACCEPTED");
    }

    @Test
    public void testGetProductsEndpoint_getById() throws Exception {
        // Arrange
        String testId = "1";
        Product product = new Product(testId, "testName", 1, 2.2);
        Mockito.when(ITestBeanFactory.getMockProductsDao().findById("1")).thenReturn(Optional.of(product));

        final MockHttpServletRequestBuilder requestBuilder =
                get("/products/" + testId)
                        .contentType(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        Assert.assertNotNull(result);
        Product receivedProduct = objectMapper.readValue(result.getResponse().getContentAsString(),
                Product.class);
        Assert.assertNotNull(receivedProduct);
        assertEquals(product.getId(), receivedProduct.getId());
    }

    @Test
    public void testDeleteProductsEndpoint_deleteOne() throws Exception {
        // Arrange
        String testId = "1";
        Mockito.when(ITestBeanFactory.getMockProductRequestQue().send(
                Mockito.any(RequestTypes.class), Mockito.any(DeleteProductRequest.class), Mockito.anyInt())).thenReturn(Optional.of(true));

        final MockHttpServletRequestBuilder requestBuilder =
                delete("/products/" + testId)
                        .contentType(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isAccepted())
                .andReturn();

        // Assert
        Assert.assertNotNull(result);
        Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                Response.class);
        Assert.assertNotNull(response);
        assertEquals(response.getStatus(), "ACCEPTED");
    }

}