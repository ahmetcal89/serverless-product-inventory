package com.assignment.inventory.repository.dao;

import com.assignment.inventory.repository.custom.CustomProductsRepositoryMethods;
import com.assignment.inventory.repository.entity.Product;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@Profile("!test")
@EnableScan
public interface ProductsDao extends CrudRepository<Product, String>, CustomProductsRepositoryMethods {

    List<Product> findAll();
}
