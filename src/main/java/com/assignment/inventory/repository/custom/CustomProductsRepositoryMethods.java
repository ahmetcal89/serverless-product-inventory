package com.assignment.inventory.repository.custom;

import com.assignment.inventory.repository.entity.Product;

import java.util.List;

public interface CustomProductsRepositoryMethods {

    List<Product> findByName(String name);

}
