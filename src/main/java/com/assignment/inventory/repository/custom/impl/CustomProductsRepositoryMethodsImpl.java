package com.assignment.inventory.repository.custom.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.assignment.inventory.repository.custom.CustomProductsRepositoryMethods;
import com.assignment.inventory.repository.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class CustomProductsRepositoryMethodsImpl implements CustomProductsRepositoryMethods {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public List<Product> findByName(String name) {

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS(name));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("name = :val1").withExpressionAttributeValues(eav);

        List<Product> scanResult;
        try {
            scanResult = dynamoDBMapper.scan(Product.class, scanExpression);
        } catch (Exception ex) {
            log.error("Error on db scan! Cause: {}", ex.getMessage());
            log.debug("Stacktrace: " + ex.getStackTrace().toString());
            return null;
        }

        return scanResult;
    }

}
