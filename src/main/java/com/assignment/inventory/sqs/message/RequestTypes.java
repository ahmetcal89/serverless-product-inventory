package com.assignment.inventory.sqs.message;

public enum RequestTypes {
    CREATE_PRODUCT_REQUEST,
    DELETE_PRODUCT_REQUEST
}
