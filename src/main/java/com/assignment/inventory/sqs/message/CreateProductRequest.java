package com.assignment.inventory.sqs.message;

import com.assignment.inventory.model.ProductForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateProductRequest {
    private String requestId;
    private ProductForm productForm;
}
