package com.assignment.inventory.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.assignment.inventory.exception.InternalServerException;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@Profile("!test")
@Slf4j
public class ProductRequestQue {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Value("${aws.sqs.productsQueName}")
    private String queName;
    @Value("${aws.accountId}")
    private String accountId;
    @Value("${aws.region}")
    private String region;
    @Value("${aws.environment}")
    private String environment;
    private AmazonSQS sqs;

    @PostConstruct
    public void initizlize() {
        sqs = AmazonSQSClientBuilder.defaultClient();
        log.info("SQS initialized for env: {}, accountId: {}, region: {}, queName: {}", environment, accountId, region, queName);
    }

    private String getQueUrl() {
        return "https://sqs." + region + ".amazonaws.com/" + accountId + "/" + queName;
    }

    public Optional<Boolean> send(RequestTypes type, Object request, int visibilityTimeout) {
        String messageBody = null;
        try {
            messageBody = objectMapper.writeValueAsString(request);
        } catch (JsonProcessingException ex) {
            throw new InternalServerException("Error during request json serialize!");
        }

        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        messageAttributes.put("Type", new MessageAttributeValue().withStringValue(type.toString()).withDataType("String"));

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(getQueUrl())
                .withMessageBody(messageBody)
                .withMessageAttributes(messageAttributes)
                .withDelaySeconds(visibilityTimeout);
        SendMessageResult result = sqs.sendMessage(send_msg_request);
        int httpStatus = result.getSdkHttpMetadata().getHttpStatusCode();
        log.info("SQS Send HTTP Status Code: {}", httpStatus);
        return Optional.of(true);
    }
}
