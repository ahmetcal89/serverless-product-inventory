package com.assignment.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForm {
    @NotNull
    private String name;
    @NotNull
    private Integer quantity;
    @NotNull
    private Double price;

}
