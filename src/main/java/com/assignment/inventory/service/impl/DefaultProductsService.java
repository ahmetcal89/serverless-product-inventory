package com.assignment.inventory.service.impl;

import com.assignment.inventory.exception.ElementNotFoundException;
import com.assignment.inventory.exception.InternalServerException;
import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.dao.ProductsDao;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.service.ProductsService;
import com.assignment.inventory.sqs.ProductRequestQue;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class DefaultProductsService implements ProductsService {

    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private ProductRequestQue productRequestQue;

    @Override
    public List<Product> listProducts() {
        return productsDao.findAll();
    }

    @Override
    public String createProductRequest(ProductForm productForm) {
        final String requestId = UUID.randomUUID().toString();
        final CreateProductRequest request = new CreateProductRequest(requestId, productForm);
        productRequestQue.send(RequestTypes.CREATE_PRODUCT_REQUEST, request, 0)
                .orElseThrow(() -> new InternalServerException("Error during SQS enqueue!"));
        return requestId;
    }

    @Override
    public Product getById(String productId) {
        return productsDao.findById(productId).orElseThrow(() -> new ElementNotFoundException("Could not find product with ID provided"));
    }

    @Override
    public String deleteProductRequest(String productId) {
        final String requestId = UUID.randomUUID().toString();
        final DeleteProductRequest request = new DeleteProductRequest(requestId, productId);
        productRequestQue.send(RequestTypes.DELETE_PRODUCT_REQUEST, request, 0)
                .orElseThrow(() -> new InternalServerException("Error during SQS enqueue!"));
        return requestId;
    }
}
