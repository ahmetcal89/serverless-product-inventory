package com.assignment.inventory.service.impl;

import com.assignment.inventory.repository.dao.ProductsDao;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.service.SupplyChainService;
import com.assignment.inventory.sqs.ProductRequestQue;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.assignment.inventory.supply.SupplyChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;


@Service
@Slf4j
public class DefaultSupplyChainService implements SupplyChainService {

    @Autowired
    private SupplyChain supplyChain;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private ProductRequestQue productRequestQue;

    @Override
    public void processCreateProductRequest(CreateProductRequest message) {
        try {
            Product product = supplyChain.saveProduct(message.getProductForm());
            productsDao.save(product);
        } catch (HttpServerErrorException ex) {
            // Try to create again after 5 sec.
            productRequestQue.send(RequestTypes.CREATE_PRODUCT_REQUEST, message, 5);
            log.warn("Supply chain server not available, try request again for request_id: {}", message.getRequestId());
        } catch (HttpClientErrorException ex) {
            log.error("Rest client error: {}", ex.getLocalizedMessage());
        }
    }

    @Override
    public void processDeleteProductRequest(DeleteProductRequest message) {
        try {
            supplyChain.deleteProduct(message.getProductId());
            productsDao.deleteById(message.getProductId());
        } catch (HttpServerErrorException ex) {
            // Try to delete again after 5 sec.
            productRequestQue.send(RequestTypes.DELETE_PRODUCT_REQUEST, message, 5);
            log.warn("Supply chain server not available, try request again for request_id: {}", message.getRequestId());
        } catch (HttpClientErrorException ex) {
            if (ex.getRawStatusCode() == HttpStatus.NOT_FOUND.value()) {
                productsDao.deleteById(message.getProductId());
            }
            log.error("Rest client error: {}", ex.getLocalizedMessage());
        }
    }

    @Override
    public void syncProducts() {
        List<Product> productList = null;
        try {
            productList = supplyChain.listProducts();
        } catch (HttpServerErrorException ex) {
            // Try to delete again after 5 sec.
            log.warn("Supply chain server not available, sync db operation will be triggered after 5 min");
            return;
        }

        productList.stream().forEach(p -> productsDao.save(p));
    }
}
