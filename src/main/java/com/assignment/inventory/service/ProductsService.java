package com.assignment.inventory.service;

import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.entity.Product;

import java.util.List;

public interface ProductsService {

    List<Product> listProducts();

    String createProductRequest(ProductForm productForm);

    Product getById(String productId);

    String deleteProductRequest(String productId);
}
