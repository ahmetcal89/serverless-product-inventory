package com.assignment.inventory.service;

import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;

public interface SupplyChainService {
    void processCreateProductRequest(CreateProductRequest message);

    void processDeleteProductRequest(DeleteProductRequest message);

    void syncProducts();
}
