package com.assignment.inventory.controller;

import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.model.Response;
import com.assignment.inventory.repository.entity.Product;
import com.assignment.inventory.service.ProductsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@RestController
@EnableWebMvc
@Slf4j
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @RequestMapping(path = "/products",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<Product> getProducts() {
        return productsService.listProducts();
    }

    @RequestMapping(path = "/products",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Response createProduct(@Validated @RequestBody ProductForm productForm) {
        Response response = new Response();
        response.setStatus("ACCEPTED");
        response.setMessage("Product Creation Request Accepted");
        response.setRequestId(productsService.createProductRequest(productForm));
        return response;
    }

    @RequestMapping(path = "/products/{product_id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Product getProductById(@PathVariable("product_id") String productId) {
        return productsService.getById(productId);
    }

    @RequestMapping(path = "/products/{product_id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Response deleteProduct(@PathVariable("product_id") String productId) {
        Response response = new Response();
        response.setStatus("ACCEPTED");
        response.setMessage("Product Delete Request Accepted");
        response.setRequestId(productsService.deleteProductRequest(productId));
        return response;
    }
}
