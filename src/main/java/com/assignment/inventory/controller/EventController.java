package com.assignment.inventory.controller;

import com.amazonaws.serverless.proxy.internal.LambdaContainerHandler;
import com.assignment.inventory.StreamLambdaHandler;
import com.assignment.inventory.service.SupplyChainService;
import com.assignment.inventory.sqs.message.CreateProductRequest;
import com.assignment.inventory.sqs.message.DeleteProductRequest;
import com.assignment.inventory.sqs.message.RequestTypes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@EnableWebMvc
@Slf4j
public class EventController {

    private final ObjectMapper objectMapper = LambdaContainerHandler.getObjectMapper();
    private final String sqsEvent = StreamLambdaHandler.AnnotatedSQSEvent.class.getName();
    private final String scheduledEvent = StreamLambdaHandler.AnnotatedScheduledEvent.class.getName();
    private final String supplyChainSyncEvent = "SupplyChainServiceEventsRule";

    @Autowired
    private SupplyChainService supplyChainService;

    @RequestMapping(path = "/event",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> handleEvent(@RequestParam("type") String type, @RequestBody String request) throws JsonProcessingException {
        if (scheduledEvent.contentEquals(type)) { // CloudWatch Events
            processCloudwatchEvent(request);
        } else if (sqsEvent.contentEquals(type)) { // SQS Events
            processSqsMessage(request);
        } else {
            log.error("Unknown event received: {}", type);
        }

        // Return Status OK not to receive duplicate messages
        Map<String, String> result = new HashMap<>();
        result.put("status", "OK");
        return result;
    }

    private void processCloudwatchEvent(String request) throws JsonProcessingException {
        log.trace("CloudWatch Event received: {}", request);

        String resource = parseEventSource(request);
        if (resource.contains(supplyChainSyncEvent)) {
            supplyChainService.syncProducts();
        } else {
            log.debug("Lambda Keep Alive Event Received");
        }
    }

    private void processSqsMessage(String request) throws JsonProcessingException {
        log.trace("SQS Message received: {}", request);

        RequestTypes messageType = parseMessageType(request);
        if (messageType == RequestTypes.CREATE_PRODUCT_REQUEST) {
            supplyChainService.processCreateProductRequest(parseCreateProductMessage(request));
        } else if (messageType == RequestTypes.DELETE_PRODUCT_REQUEST) {
            supplyChainService.processDeleteProductRequest(parseDeleteProductMessage(request));
        } else {
            log.error("Invalid SQS Message Received: {}", request);
        }
    }

    private CreateProductRequest parseCreateProductMessage(String request) throws JsonProcessingException {
        ObjectReader objectReader = objectMapper.readerFor(CreateProductRequest.class);
        return objectReader.readValue(parseMessageBody(request));
    }

    private DeleteProductRequest parseDeleteProductMessage(String request) throws JsonProcessingException {
        ObjectReader objectReader = objectMapper.readerFor(DeleteProductRequest.class);
        return objectReader.readValue(parseMessageBody(request));
    }

    private String parseMessageBody(String request) throws JsonProcessingException {
        Map<String, Object> raw = (Map<String, Object>) objectMapper.readValue(request, Map.class);
        String body = "";

        // Peek inside first record
        if (raw.get("Records") instanceof List) {
            List<Map<String, Object>> events = (List<Map<String, Object>>) raw.get("Records");
            if (events.size() > 0) {
                body = events.get(0).get("body").toString();
            } else {
                log.error("Empty, dummy event records {}", events);
            }
        }

        return body;
    }

    private String parseEventSource(String request) throws JsonProcessingException {
        Map<String, Object> raw = (Map<String, Object>) objectMapper.readValue(request, Map.class);
        String resource = "";

        // Peek inside first record
        if (raw.get("resources") instanceof List) {
            List<String> resources = (List<String>) raw.get("resources");
            if (resources.size() > 0) {
                resource = resources.get(0);
            } else {
                log.error("Empty, dummy event records {}", resources);
            }
        }

        return resource;
    }


    private RequestTypes parseMessageType(String request) throws JsonProcessingException {
        Map<String, Object> raw = (Map<String, Object>) objectMapper.readValue(request, Map.class);

        // Peek inside first record
        if (raw.get("Records") instanceof List) {
            List<Map<String, Object>> events = (List<Map<String, Object>>) raw.get("Records");
            if (events.size() > 0) {
                raw = events.get(0);
            } else {
                log.warn("Empty, dummy event records {}", events);
                return RequestTypes.valueOf("UNDEFINED");
            }
        }

        Map<String, Map<String, String>> messageAttributes = (Map<String, Map<String, String>>) raw.get("messageAttributes");
        String messageType = messageAttributes.get("Type").get("stringValue");
        return RequestTypes.valueOf(messageType);
    }

    @ExceptionHandler(JsonProcessingException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    String handleJsonProcessingException(final JsonProcessingException ex,
                                         final HttpServletRequest request) {
        return ex.getLocalizedMessage();
    }
}
