package com.assignment.inventory.supply;

import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.entity.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class MockSupplyChain implements SupplyChain {

    // TODO: Move to application.properties file
    @Value("${supplyChain.endpoint}")
    private String supplyChainEndpoint;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RestTemplate restTemplate;

    @SneakyThrows
    @Override
    public List<Product> listProducts() {
        return restTemplate.getForObject(supplyChainEndpoint, SupplyBundle.class).getBundle();
    }

    @SneakyThrows
    @Override
    public Product saveProduct(ProductForm product) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request =
                new HttpEntity<String>(objectMapper.writeValueAsString(product), headers);

        return restTemplate.postForObject(supplyChainEndpoint, request, Product.class);
    }

    @Override
    public Product getById(String productId) {
        return restTemplate.getForObject(supplyChainEndpoint.concat("/").concat(productId), Product.class);
    }

    @Override
    public void deleteProduct(String productId) {
        restTemplate.delete(supplyChainEndpoint.concat("/").concat(productId));
    }


}
