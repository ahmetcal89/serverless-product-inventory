package com.assignment.inventory.supply;

import com.assignment.inventory.repository.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SupplyBundle {

    private List<Product> bundle;
}
