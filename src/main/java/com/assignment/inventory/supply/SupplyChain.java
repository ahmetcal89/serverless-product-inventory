package com.assignment.inventory.supply;

import com.assignment.inventory.model.ProductForm;
import com.assignment.inventory.repository.entity.Product;

import java.util.List;

public interface SupplyChain {
    List<Product> listProducts();

    Product saveProduct(ProductForm product);

    Product getById(String productId);

    void deleteProduct(String productId);
}
